(function ($) {
    window.onload = function () {
        const conf_app = {
            variables: {
                userData: [],
            },

            getMathTotal: () => Math.floor(Math.random() * 100),
            setDisplayFoo: (prop) => {
                $("#loader-img").css("display", prop);
            },

            fetchUsers: async () => {
                conf_app.setDisplayFoo("block");
                await fetch(`https://randomuser.me/api/?results=${conf_app.getMathTotal()}`)
                    .then(response => response.json())
                    .then(r => conf_app.variables.userData = r.results)
                conf_app.setDisplayFoo("none");
                conf_app.showTable();
                conf_app.showStatistics();
            },

            showTable: () => {
                console.log("conf_app.variables.userData ", conf_app.variables.userData)
                const { userData } = conf_app.variables;
                for (let i = 0; i < userData.length; i++) {
                    const wrapElem = `<div class="card">
                                        <div class="card__names">
                                            <div class="card__name">${userData[i].name.first} ${userData[i].name.last}</div>
                                            <div class="card__gender">Gender: ${userData[i].gender}</div>
                                            <div class="card__number">Number: ${userData[i].cell}</div>
                                            <div class="card__email">Email: ${userData[i].email}</div>
                                            <div class="card__email">Adress: 
                                                ${userData[i].location.country}
                                                ${userData[i].location.state}
                                                ${userData[i].location.city}
                                                ${userData[i].location.street.name}
                                                ${userData[i].location.street.number}
                                            </div>
                                        </div>
                                        <div class="card__info">
                                            
                                        </div>
                                      </div>`;
                    $("#table").prepend(wrapElem);
                }
            },

            showStatistics: () => {
                const { userData } = conf_app.variables;

                const totalUsers = userData.length;
                const totalUsersMens = userData.filter((item) => item.gender === "male").length;
                const totalUsersWomen = totalUsers - totalUsersMens;
                const TR = userData.filter((item) => item.nat === "TR").length;
                const NZ = userData.filter((item) => item.nat === "NZ").length;
                const ES = userData.filter((item) => item.nat === "ES").length;
                const BR = userData.filter((item) => item.nat === "BR").length;
                const FI = userData.filter((item) => item.nat === "FI").length;
                const NO = userData.filter((item) => item.nat === "NO").length;
                const FR = userData.filter((item) => item.nat === "FR").length;
                const IE = userData.filter((item) => item.nat === "IE").length;
                const CH = userData.filter((item) => item.nat === "CH").length;
                const NL = userData.filter((item) => item.nat === "NL").length;
                const DK = userData.filter((item) => item.nat === "DK").length;
                const US = userData.filter((item) => item.nat === "US").length;
                const GB = userData.filter((item) => item.nat === "GB").length;
                const AU = userData.filter((item) => item.nat === "AU").length;
                const CA = userData.filter((item) => item.nat === "CA").length;

                const wrapElem = `<div class="total">
                                    <div class="total__total">total Users: ${totalUsers}</div>
                                    <div class="total__gender">Men: ${totalUsersMens} Women: ${totalUsersWomen}</div>
                                    <div class="total__count">Count: ${conf_app.getGender(totalUsersMens, totalUsersWomen)}</div>
                                    <div class="total__gender">Nations: 
                                        TR: ${TR},
                                        NZ: ${NZ},
                                        ES: ${ES},
                                        BR: ${BR},
                                        FI: ${FI},
                                        NO: ${NO},
                                        FR: ${FR},
                                        IE: ${IE},
                                        CH: ${CH},
                                        NL: ${NL},
                                        DK: ${DK},
                                        US: ${US},
                                        GB: ${GB},
                                        AU: ${AU},
                                        CA: ${CA}
                                    </div>                                    
                                </div>`;
                $("#statistics").prepend(wrapElem);
            },
            getGender: (a, b) => {
                return a > b ? "men more than women" : "women more than men"
            },

            setHandlers: () => {
                $("#load").on("click", e => {
                    conf_app.fetchUsers();
                });
            },

            init: () => {
                conf_app.setHandlers();
            }
        };

        conf_app.init();
    };
})(jQuery);
